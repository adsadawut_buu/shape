/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.shapeproject;

/**
 *
 * @author อัษฎาวุฒิ
 */
public class TestRectangle {
    public static void main(String[] args) {
        Rectangle rectangle1 = new Rectangle(4,3);
        System.out.println("Area of rectangle1(l = "+ rectangle1.getL()+"),(w = "+ rectangle1.getW()+") is "+rectangle1.recArea());
        rectangle1.setLW(2,3);
        System.out.println("Area of rectangle1(l = "+ rectangle1.getL()+"),(w = "+ rectangle1.getW()+") is "+rectangle1.recArea());
        rectangle1.setLW(rectangle1.getL(),4);
        System.out.println("Area of rectangle1(l = "+ rectangle1.getL()+"),(w = "+ rectangle1.getW()+") is "+rectangle1.recArea());
        rectangle1.setLW(0,0);
        System.out.println("Area of rectangle1(l = "+ rectangle1.getL()+"),(w = "+ rectangle1.getW()+") is "+rectangle1.recArea());
        System.out.println(rectangle1.toString());
    }
}
