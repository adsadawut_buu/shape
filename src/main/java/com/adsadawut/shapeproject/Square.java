/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.shapeproject;

/**
 *
 * @author อัษฎาวุฒิ
 */
public class Square {
    private double a;
    public Square(double a){
        this.a=a;
    }
    public double squareArea(){
        return a*a;
    }
    public double getA(){
        return a;
    }
    public void setA(double a){
        if(a<=0){
            System.out.println("Error: Side must more than zero!!!");
            return;
        }
        this.a=a;
    }
    public String toString(){
        return "Area of square (a = " + this.getA() +") is "+ this.squareArea();
    }
}
