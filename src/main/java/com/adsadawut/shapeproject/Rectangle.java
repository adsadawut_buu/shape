/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.shapeproject;

/**
 *
 * @author อัษฎาวุฒิ
 */
public class Rectangle {
    private double l;
    private double w;
    public Rectangle(double l,double w){
        this.l=l;
        this.w=w;
    }
    public double recArea(){
        return l*w;
    }
    public double getL(){
        return l;
    }
    public double getW(){
        return w;
    }
    public void setLW(double l,double w){
        if(l<=0 || w<=0){
            System.out.println("Error: Length or Width must more than zero!!!");
            return;
        }
        this.l=l;
        this.w=w;
    }public String toString(){
        return "Area of rectangle1(l = "+ this.getL()+"),(w = "+ this.getW()+") is "+this.recArea();
    }
}
