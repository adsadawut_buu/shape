/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.shapeproject;

/**
 *
 * @author อัษฎาวุฒิ
 */
public class Triangle {
    private double h ;
    private double b ;
    public Triangle(double h,double b){
        this.h=h;
        this.b=b;
    }public double triangleArea(){
        return (h*b)/2;
    }
    public double getH(){
        return h;
    }
    public double getB(){
        return b;
    }
    public void setHB(double h,double b){
        if(h<=0 || b<=0){
            System.out.println("Error: Height or Base must more than zero!!!");
            return;
        }
        this.h=h;
        this.b=b;
    }
    public String toString(){
        return "Area of triangle (h = "+this.getH()+"),(b = "+this.getB()+") is "+this.triangleArea();
    }
}
